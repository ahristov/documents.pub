#! /usr/bin/env bash

rm -rf index.mdown
rm -rf index.html

echo "
# Notebook

" >> index.mdown

for mdownfile in `ls *.mdown | sort`
do

	command="echo ${mdownfile} | sed -e 's/\\.mdown/\\.html/'"
	htmlfile=`eval $command`

	command="echo ${mdownfile} | sed -e 's/\\.mdown//'"
	linkname=`eval $command`

	[[ "$linkname" == "index" ]] && continue

	echo "Running ${command}"
	echo "Compiling ${mdownfile} into ${htmlfile} ..."

	markdown $mdownfile -x toc > $htmlfile


	echo "
* [${linkname}](${htmlfile})
" >> index.mdown

done


echo "
---
" >> index.mdown


markdown index.mdown > index.html


